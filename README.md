# operations-recovery-runbook
## Description
This project demonstrates many often overlooked aspects of running an operationally excellent, 
production grade system; principally focused on recovering from errors.  There is a medium article
that discusses these topics in depth here TODO link

This project is divided into two "stacks", which would normally be held in separate repositories.  You 
would likely have one "recovery" stack that is used across your entire aws account/gcp project, and then
you would have many different application stacks.  However, for the sake of this demo, I put them both in 
this repo, just under separate folders ("application" and "recovery" respectively).  There is a README in
each of the stacks containing more specific information.

## Usage
TODO example of script running to get both up and running

## Project status
This project is "complete", and I will not be adding to it.  It merely exists to demonstrate concepts for my article.