# defines providers and GCP account info
provider "google" {
  region = var.region
  project = var.gcp_project
}

provider "google-beta" {
  region = var.region
  project = var.gcp_project
}