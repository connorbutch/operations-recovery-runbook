variable "region" {
  type        = string
  description = "The GCP region the resources are launched intp"
  default = "us-west4"
}

variable "gcp_project" {
  description = "The GCP project id you would like to launch resources into"
  type        = string
}
