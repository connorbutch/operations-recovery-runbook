resource "google_pubsub_topic" "metric_topic" {
  name = "example-topic"
}

resource "google_pubsub_topic" "metric_topic_dlq" {
  name = "example-topic-dlq"
  //must set this to keep messages if there are no subscriptions on our topic (which is the default)
  message_retention_duration = "86600s"
}

resource "google_pubsub_subscription" "example_function_subscription" {
  name  = "example-subscription"
  topic = google_pubsub_topic.metric_topic.name
  push_config {
    push_endpoint = google_cloudfunctions_function.example_function.https_trigger_url
  }
}

resource "google_cloudfunctions_function" "example_function" {
  name                  = "example-function"
  project               = var.gcp_project
  runtime               = "java11"
  timeout               = 60
  available_memory_mb   = 256
  source_archive_bucket = "tm-artifacts" //TODO
  source_archive_object = "datadog/archive.zip" //TODO
  entry_point           = "com.ford.pro.metrics.PublishMetrics" //TODO #Edit entrypoint
  environment_variables = {
    GCP_PROJECT_ID = var.gcp_project
  }
  timeouts {
    create = "10m"
    update = "10m"
  }
  trigger_http = true
  service_account_email = module.example_service_account.email
}

module "example_service_account" {
  source = "github.com/terraform-google-modules/terraform-google-service-accounts"
  description = "Service account to be used to save to firestore"
  project_id = var.gcp_project
  display_name = "example-function-sa"
  names = [
    "example-function-sa"
  ]
  project_roles = [
    "${var.gcp_project}=>roles/secretmanager.secretAccessor",
    "${var.gcp_project}=>roles/firebase.admin"
  ]
}