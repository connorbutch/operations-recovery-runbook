# operations-recovery-runbook
## Description
This part of the project demonstrates an example application that consists of a single cloud function
that is triggered by a pubsub subscription that, when working correctly, saves to firestore.  When it
does not work, the subscription has a dlq configured.

In order to create errors in processing, if the secret "doThrowErrors" is set to true in secrets manager, the function
will throw an exception.  This lets us test our operational processes for recovering.

## Usage
Go to the pubsub topic (example topic) in the console and click send message to fire the function.
To simulate errors, go to secrets manager and set the value of the  secret with key "doThrowErrors" to 
"true".  Anything but "true", and the function will process the messages.

## Project status
This project is "complete", and I will not be adding to it.  It merely exists to demonstrate concepts for my article.