# operations-recovery-runbook
## Description
This part of the project demonstrates the recovery aspect.  Normally, you could create one of these
stacks per gcp project/aws account.   This stack consists of a few components:

1 - a function (and supporting components) that is triggered by http; which creates a temporary subscription
between a dlq and an original queue.  This function takes in the queue name, how long to create the subscription, and 
the max number of functions to use to reprocess (prevents queue from getting too many old messages at once from the dlq).  
It creates the subscription with a special label (isRecovery:true) and another label for deletion time 
(as well as a cloud function to subscribe to the queue), so that we can automatically disable it.

2 - a function that is triggered by event arc (gcp equivalent of eventbridge/cloudwatch events) when a new subscription 
is created.  It inspects the subscription, and if it has the special label (isRecovery:true), then it 
schedules a cloud task to run to delete the subscription after a certain period of time

3 - same as above, except that it listens to cloud function creation and the function rather than the subscription

4 - a function that is fired by cloud tasks and deletes a function

5 - a function that is fired by cloud tasks and deletes a subscription

## Usage
To start the recovery process, grab the endpoint from the output of the terraform, and paste it in the url of the 
included postman collection.  Then send the request.
TODO include the postman collection here

## Project status
This project is "complete", and I will not be adding to it.  It merely exists to demonstrate concepts for my article.