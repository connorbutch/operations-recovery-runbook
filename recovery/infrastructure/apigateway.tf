resource "google_project_service" "apigateway" {
  project = var.gcp_project
  service = "apigateway.googleapis.com"

  disable_dependent_services = false
  disable_on_destroy = false
}

resource "google_project_service" "servicecontrol" {
  project = var.gcp_project
  service = "servicecontrol.googleapis.com"

  disable_dependent_services = false
  disable_on_destroy = false
}

module "recovery_apigw_service_account" {
  source = "github.com/terraform-google-modules/terraform-google-service-accounts"
  description = "Service account used by api gateway to invoke the recovery function"
  project_id = var.gcp_project
  display_name = "recovery-apigw-sa"
  names = [
    "recovery-apigw-sa"
  ]
  project_roles = [
    "${var.gcp_project}=>roles/pubsub.admin"
  ]
}

resource "google_api_gateway_api" "recovery_api_gateway" {
  provider = google-beta
  api_id = "recovery-api"

  depends_on = [
    google_project_service.apigateway
  ]
}

resource "google_api_gateway_api_config" "recovery_api_cfg" {
  provider = google-beta
  api = google_api_gateway_api.recovery_api_gateway.api_id
  api_config_id = "${formatdate("YYYYMMDDhhmmss",timestamp())}-cfg"
  openapi_documents {
    document {
      path = "spec.yaml"
      contents = base64encode(templatefile("openapi-spec.yml", {
        recovery_function_url = google_cloudfunctions_function.create_temp_sub_function.https_trigger_url
      }))
    }
  }
  lifecycle {
    create_before_destroy = true
  }
  gateway_config {
    backend_config {
      google_service_account = module.recovery_apigw_service_account.email
    }
  }
}

resource "google_api_gateway_gateway" "api_gw" {
  provider = google-beta
  api_config = google_api_gateway_api_config.recovery_api_cfg.id
  gateway_id = google_api_gateway_api.recovery_api_gateway.api_id
}