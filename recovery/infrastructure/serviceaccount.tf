module "recovery_service_account" {
  source = "github.com/terraform-google-modules/terraform-google-service-accounts"
  description = "Service account to be used to create temporary subscriptions from dlq to original queue"
  project_id = var.gcp_project
  display_name = "recovery-function-sa"
  names = [
    "recovery-function-sa"
  ]
  project_roles = [
    "${var.gcp_project}=>roles/pubsub.admin"
  ]
}

module "delete_service_account" {
  source = "github.com/terraform-google-modules/terraform-google-service-accounts"
  description = "Service account to be used to automatically delete temporary subscriptions from dlq to original queue after a given period of time"
  project_id = var.gcp_project
  display_name = "delete-function-sa"
  names = [
    "delete-function-sa"
  ]
  project_roles = [
    "${var.gcp_project}=>roles/pubsub.admin"
  ]
}