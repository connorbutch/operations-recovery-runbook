# defines providers and GCP account info
provider "google" {
  region = var.region
  project = "tenant-mgmt-${var.environment}"
}

provider "google-beta" {
  region = var.region
  project = "tenant-mgmt-${var.environment}"
}