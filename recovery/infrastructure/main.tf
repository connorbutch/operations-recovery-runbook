resource "google_cloudfunctions_function" "create_temp_sub_function" {
  name = "create-temp-sub-function"
  project = var.gcp_project
  runtime = "java11"
  timeout = 60
  available_memory_mb = 256
  source_archive_bucket = "tm-artifacts"
  //TODO
  source_archive_object = "datadog/archive.zip"
  //TODO
  entry_point = "com.ford.pro.metrics.PublishMetrics"
  //TODO #Edit entrypoint
  environment_variables = {
    GCP_PROJECT_ID = var.gcp_project
  }
  timeouts {
    create = "10m"
    update = "10m"
  }
  trigger_http = true
  service_account_email = module.recovery_service_account.email
}
//TODO event arc function -- fired when subscription with dlq tag is created; and schedules a cloud task at expiration time



//TODO function fired off timer (just needs to be triggered by http from cloud tasks); will use delete service account created above
//https://cloud.google.com/tasks/docs/dual-overview

//TODO service account with cloud task (not cloud scheduler) admin